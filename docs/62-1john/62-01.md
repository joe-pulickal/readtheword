# 1

??? summary "Seven arrows"
    |Arrow|Question|
    |----:|:-------|
    | ⟲  | What does this passage say?|
    | 🠔  | What did this passage mean to its original audience?|
    | ↑   | What does this passage tell us about God?|
    | ↓   | What does this passage tell us about man? |
    | →   | What does this passage demand of me? |
    | ⇋   | How does this passage change the way I relate to people?|
    | ↺   | How does this passage prompt me to pray to God? |
